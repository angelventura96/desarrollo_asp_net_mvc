﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EJERCICIO2_REST_CRUD_CLIENTE.Models
{
    [MetadataType(typeof(Cliente.MetaData))]
    public partial class Cliente //Para añadir funcionalidades a nuestro modelo/clase Cliente (tablaBD), en tiempo de ejecucion, DecoratorPattern
    {
        sealed class MetaData //Para que no se pueda instanciar la clase, y los datos que se añaden en tiempo de ejecucion
        {
            [Key]
            public int Clienteid;

            [Required(ErrorMessage = "Ingresa el nombre del cliente")]
            public string Clientenombre;

            [Required(ErrorMessage = "Ingresa el credito que tendra el cliente")]
            public Nullable<int> Creditocliente;

            [Required(ErrorMessage = "Ingresa la edad del cliente")]
            [Range(18,70)]
            public Nullable<int> Edadcliente;

            [Required(ErrorMessage = "Ingresa el correo electronico del cliente")]
            [EmailAddress(ErrorMessage = "El formato ingresado de correo es invalido")]
            public string Correocliente;
        }
    }
}