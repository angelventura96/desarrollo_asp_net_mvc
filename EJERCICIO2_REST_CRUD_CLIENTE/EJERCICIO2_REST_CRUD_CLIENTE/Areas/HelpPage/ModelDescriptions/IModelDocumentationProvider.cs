using System;
using System.Reflection;

namespace EJERCICIO2_REST_CRUD_CLIENTE.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}