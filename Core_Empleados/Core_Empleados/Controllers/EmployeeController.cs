﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Empleados.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Empleados.Controllers
{
    public class EmployeeController : Controller
    {
        //variable _db para acceder a la base de datos, una vez inicializada no podra cambiarse, es de tipo ApplicationDbContext
        private readonly ApplicationDbContext _db;

        public EmployeeController(ApplicationDbContext db) //cosntructor de la clase
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Employee.ToList(); // la variable displaydata almacenara en lista todos los registros de la tabla/modelo Employee
            return View(displaydata); //la vista regresara lo que contenga la variable displaydata
        }

        [HttpGet]
       //sobrecarga del metodo index recibiendo como parametro empsearch que es el value de nuestro input search
        public async Task<IActionResult> Index(string empSearch)
        {
            ViewData["GetEmployeesDetails"] = empSearch; //empsearch contiene el valor ingresado en el inputsearch
            var empquery = from x in _db.Employee select x;//se hace un select *, empqeury contendra el resultado
            if (!String.IsNullOrEmpty(empSearch))//si ese resultado no es vacio o nulo, es decir que si haya resultados, 
                                                 //y añadimos la clausula where en el query para delimitar a un solo empleado
            {
                //where , donde el registro contenga el string ingresado en el input search, en los campos nombre, o email.
                empquery = empquery.Where(x => x.Empname.Contains(empSearch) || x.Email.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());//recarga la pagina index para regresar todos los empleados select *
        }

        public IActionResult Create() //Muestra la vista con el formulario para crear un nuevo empleado
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Employee newEmp) //Est funcion se ejecuta cuando se le da "submit" al formulario, sera asincrono
        {
            if (ModelState.IsValid) //si el modelo (datos del formulario) es valido
            {
                _db.Add(newEmp); //agregara los datos del modelo (formulario) a la bd
                await _db.SaveChangesAsync(); //espera a que se guarden los datos en la bd 
                return RedirectToAction("Index"); //regresa a la pagina index (lista de empleados)
            }
            return View(newEmp); //si el modelo es invalido, regresa a la vista que se llame igual que el metodo (create, linea 26), pero no 
                                 //regresa el formulario vacio, si no con los datos del model
        }

        public async Task<IActionResult> Detail(int? id) //el ? es por que puede o no recibir el paramatro
        {
            if(id == null)
            {
                return RedirectToAction("Index"); //si el id es nulo, nos redirige a la pantalla principal
            }
            var getEmpDetail = await _db.Employee.FindAsync(id); //Cachamos en getEmpDetail el resultado del select del id del parametro
            return View(getEmpDetail); //rergesamos la vista con el detalle del id especificado
        }

        public async Task<IActionResult> Edit(int? id) //TASK, TIPO DE OBJETO QUE PERMITE AWAIT PARA UNA OPERACION ASYNCRONA QUE RETORNA UN VALOR
        {
            if (id == null)
            {
                return RedirectToAction("Index"); //si el id es nulo, nos redirige a la pantalla principal
            }
            var getEmpDetail = await _db.Employee.FindAsync(id); //Cachamos en getEmpDetail el resultado del select del id del parametro
            return View(getEmpDetail); //rergesamos la vista con el detalle del id especificado
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Employee oldEmp)
        {
            if(ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if(id ==  null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
