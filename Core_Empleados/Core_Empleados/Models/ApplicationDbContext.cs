﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Empleados.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Employee> Employee { get; set; }
        //Se deben de guardar en DbSet del tipo de dato de la tabla a tratar
        //el nombre del entity debe ser exactamente igual al de la tabla en la base de datos
    }
}
