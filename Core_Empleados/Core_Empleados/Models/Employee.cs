﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Core_Empleados.Models
{
    public class Employee{
        [Key] //campo autogenerado, sera el id del modelo
        [Display(Name = "ID del Empleado")]
        public int Empid { get; set; }

        [Required(ErrorMessage = "Enter the employee name")]
        [Display(Name = "Nombre del Empleado")] //display es el label del campo de texto como en HTML, form label:"Employee name"
        public string Empname { get; set; }

        [Required(ErrorMessage = "Enter the employee email")] //required, como en HTML, el campo es obligatorio, si no se da, muestra el error
        [Display(Name = "Correo electrónico")]
        [EmailAddress(ErrorMessage = "Enter a valid email address")]//validacion para correo electronico - data-annotations
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter the employee age")]
        [Display(Name = "Edad")]
        [Range(20, 50)] //validacion para que la edad solo este en esos valores
        public int Age { get; set; }

        [Required(ErrorMessage = "Enter the employee salary")]
        [Display(Name = "Salario")]
        public int Salary { get; set; }
    }
}
