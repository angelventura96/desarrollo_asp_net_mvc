﻿using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
