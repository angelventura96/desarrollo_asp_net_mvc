Create database EJERCICIO1

use EJERCICIO1;

CREATE TABLE Cliente(
	Clienteid int not null primary key identity(1,1),
	Clientenombre nvarchar(150),
	Creditocliente int,
	Edadcliente int,
	Correocliente nvarchar(150)
);

insert into Cliente(Clientenombre, Creditocliente, Edadcliente, Correocliente)
values ('Angel Ventura', 20000, 24, 'javentura96@gmail.com');

select * from Cliente;