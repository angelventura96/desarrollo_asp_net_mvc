﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_EMPLOYEE.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData
        {
            [Key]
            public int Empid;

            [Required(ErrorMessage = "Ingresa el nombre")]
            public string Empname;

            [Required(ErrorMessage = "Ingresa el correo electronico")]
            [EmailAddress(ErrorMessage = "El formato de correo no es valido")]
            public string Email;

            [Required(ErrorMessage = "Ingresa la edad")]
            [Range(18,60)]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa el salario del empleado")]
            public Nullable<int> Salary;
        }
    }
}