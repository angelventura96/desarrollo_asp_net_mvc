﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using REST_EMPLOYEE.Models;

namespace REST_EMPLOYEE.Controllers
{
    public class EmployeesController : ApiController //hereda de apicontroller
    {
        private EMPEntities db = new EMPEntities(); //instancia a la bd

        // GET: api/Employees - REGRESA TODOS LOS EMPLEADOS DE LA BASE DE DATOS
        public IQueryable<Employee> GetEmployee()
        {
            return db.Employee; //SELECT * FROM
        }

        // GET: api/Employees/5 - OBTENER A UN EMPLEADO EN ESPECIFICO, REGRESA UNA RESPUESTA DE TIPO EMPLEADO
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmployee(int id) //RECIBE COMO PARAMETRO EL ID DEL EMPLEADO  A BUSCAR
        {
            Employee employee = db.Employee.Find(id); //BUSCA AL EMPLEADO
            if (employee == null) //SI ES NULO
            {
                return NotFound(); //REGRESA 404 NOT FOUND
            }

            return Ok(employee); //REGRESA 200 OK Y EL EMPLEADO
        }

        // PUT: api/Employees/5
        [ResponseType(typeof(void))] //RGRESA 204, SE HIZO LA TAREA, PERO LA RESPEUSTA NO TIENE CONTENIDO
        public IHttpActionResult PutEmployee(int id, Employee employee) //ACTUALIZA UN EMPLEADO, RECIBE EL ID, Y LOS DATOS A ACTUALIZAR
        {
            if (!ModelState.IsValid) //VALDIA QUE LOS DATOS A ACTUALIZAR SEAN CORRECTOS
            {
                return BadRequest(ModelState);//SI NO POR EJEMPLO, UN CORREO CON FORMATO INCORRECTO, BADREQUEST
            }

            if (id != employee.Empid)//VALIDA QUE EL IDEMPLEADO A ACTUALIZAR (URL) CON EL IDEMPLEADO (BODY) SEA EL MISMO
            {
                return BadRequest(); //SI NO, BADREQUEST
            }

            db.Entry(employee).State = EntityState.Modified; //SI SI, LO MODIFICA

            try //MANEJO DE EXCEPCION
            {
                db.SaveChanges(); //INTENTA GUARDAR LOS CAMBIOS
            }
            catch (DbUpdateConcurrencyException) //SI HAY EXCEPCION QUE ALGUIEN MAS ESTE MODIFICANDO O HAYA ELOMINADO ESE EMP, CONCURRENCIA
            {
                if (!EmployeeExists(id)) //VERIFICA QUE SIGA EXISTIENDO EL EMPLEADO A GUARDAR SUS CAMBIOS
                {
                    return NotFound(); //SI NO, REGRESA 404 NOT FOUND
                }
                else
                {
                    throw; //SI AUN EXISTE PERO HAY ERRPR DE CONCURRENCIA LANZA LA EXCEPCION
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employees
        [ResponseType(typeof(Employee))] //REGRESA RESPEUSTA DE TIPO EMPLEADO
        public IHttpActionResult PostEmployee(Employee employee) //CREAR UN EMPLEADO
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employee.Add(employee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee.Empid }, employee); //REGRESA EL OBJETO DEL EMP CREADO
        }

        // DELETE: api/Employees/5
        [ResponseType(typeof(Employee))] //REGRESA EL OBJETO DEL EMPELADO ELIMINADO
        public IHttpActionResult DeleteEmployee(int id) // bORRRAR UN EMPLEADO POR ID
        {
            Employee employee = db.Employee.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employee.Remove(employee);
            db.SaveChanges();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employee.Count(e => e.Empid == id) > 0;
        }
    }
}