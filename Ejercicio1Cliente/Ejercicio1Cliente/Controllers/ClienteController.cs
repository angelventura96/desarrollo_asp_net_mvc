﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ejercicio1Cliente.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Ejercicio1Cliente.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var mostrarDatos = _db.Cliente.ToList();
            return View(mostrarDatos);
        }

        [HttpGet]
        public async Task<IActionResult> Index(String buscaCliente)
        {
            ViewData["DetalleCliente"] = buscaCliente;
            var buscaClienteQuery = from x in _db.Cliente select x;
            if(!String.IsNullOrEmpty(buscaCliente))
            {
                buscaClienteQuery = buscaClienteQuery.Where(x => x.Clientenombre.Contains(buscaCliente) || x.Correocliente.Contains(buscaCliente));
            }
            return View(await buscaClienteQuery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Cliente nuevoCliente)
        {
            if(ModelState.IsValid)
            {
                _db.Add(nuevoCliente);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nuevoCliente);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEmpleado = await _db.Cliente.FindAsync(id);
            return View(detalleEmpleado);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEmpleado = await _db.Cliente.FindAsync(id);
            return View(detalleEmpleado);
        }

        [HttpPost]
        public async Task<IActionResult> Edit (Cliente actualizarCliente)
        {
            if(ModelState.IsValid)
            {
                _db.Update(actualizarCliente);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(actualizarCliente);
        }

        public async Task<IActionResult> Delete (int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var detalleEmpleado = await _db.Cliente.FindAsync(id);
            return View(detalleEmpleado);
        }

        [HttpPost]
        public async Task<IActionResult> Delete (int id)
        {
            var detalleEmpleado = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(detalleEmpleado);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
