﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ejercicio1Cliente.Models
{
    public class Cliente
    {
        [Key]
        [Display(Name = "Id del cliente")]
        public int Clienteid { get; set; }

        [Display(Name = "Nombre del cliente")]
        [Required(ErrorMessage = "Ingresa el nombre del cliente")]
        public string Clientenombre { get; set; }

        [Display(Name = "Credito del cliente")]
        [Required(ErrorMessage = "Ingresa el credito del cliente")]
        public int Creditocliente { get; set; }

        [Display(Name = "Edad del cliente")]
        [Required(ErrorMessage = "Ingresa la edad del cliente")]
        [Range(18,70)]
        public int Edadcliente { get; set; }

        [Display(Name = "Correo del cliente")]
        [Required(ErrorMessage = "Ingresa el correo del cliente")]
        [EmailAddress(ErrorMessage = "El formato del correo ingresado no es valido")]
        public string Correocliente { get; set; }
    }
}
